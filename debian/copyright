Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: WinFF
Upstream-Contact: Matthew Weatherford <matt@biggmatt.com>
Files-Excluded: winff-windows-packaging
Comment: DFSG

Files: *
Copyright: © 2006-2016 Matthew Weatherford <matt@biggmatt.com>
License: GPL-3+

Files: *.pas
Copyright: © 2006-2016 Matthew Weatherford <matt@biggmatt.com>
  	   © 2011 Alexey Osipov <lion-simba@pridelands.ru>
License: GPL-3+

Files: winff/potranslator.pas
Copyright: © 2004-2010 V.I.Volchenko and Lazarus Developers Team
Comment: This code is copied from the wiki of lazarus
  https://wiki.freepascal.org/Translations_/_i18n_/_localizations_for_programs
  where it mentions that it originates from DefaultTranslation.pas in the
  LCL. The license of that file is given below.
License: GPL-2+

Files: winff/winff.1
Copyright: © 2008 Paul Gevers <paul@climbing.nl>
License: GPL-3+

Files: winff/docs/*
Copyright: © 2008-2009 Matthew Weatherford <matt@biggmatt.com>
License: GFDL-1.3+

Files: winff/docs/License-Documents.txt
Copyright: Copyright (C) 2000, 2001, 2002, 2007, 2008
  Free Software Foundation, Inc. <https://fsf.org/>
License: GFDL-1.3+

Files: winff/docs/WinFF.fr.*
Copyright: © 2008-2009 Thomas "Choplair" Gutleben <chopinou@choplair.org>
License: GFDL-1.3+

Files: winff/docs/WinFF.nl.*
Copyright: © 2008-2009 Paul Gevers <paul@climbing.nl>
License: GFDL-1.3+

Files: winff/docs/WinFF.pt*.*
Copyright: © 2022 marcelocripe <marcelocripe@gmail.com>
License: GFDL-1.3+

Files: debian/*
Copyright: © 2008-2016  Paul Gevers   <elbrus@debian.org>
           © 2023-2024 Peter Blackman <peter@pblackman.plus.com>
License: GPL-3+

Files:
  debian/winff.svg
  winff/winff.ico
  winff/winff-icons/*
Copyright: © 2009 Alex Klepatsch
License: GPL-3+

Files: winff/winff.desktop
Copyright:  © 2022-2023 Robin Antix  
License: GPL-3+
Comment: https://gitlab.com/Robin-antiX/antix23-desktop-files

Files: debian/presets*.xml
Copyright: © 2006-2011 Matthew Weatherford <matt@biggmatt.com>
License: GPL-3+

Files: winff/languages/*
Copyright: © 2006-2013 Matthew Weatherford <matt@biggmatt.com>
           (c) 2011-2016 Rosetta Contributors and Canonical Ltd
           © 2008-2010 Marcin Trybus <mtrybus@o2.pl>
           © 2008 Luís Torrao <luistorrao@gmail.com>
           © 2009 Joe Black <vladimir-irina@mail.ru>
  	   © 2011 Alexey Osipov <lion-simba@pridelands.ru>
License: GPL-3+

Files: winff/languages/winff.sv.po
Copyright: © 2009 Göran Hanell <hannell@algonet.se>
           © 2012 Christopher Forster
           © 2020 Åke Engelbrektson <eson@svenskasprakfiler.se>
License: GPL-3+

Files: winff/languages/winff.pt_BR.po
Copyright: © 2008-2009 Arlindo Saraiva Pereira Junior <nighto@nighto.net> 
           © 2014 Hriostat <o e-mail não foi informado>
           © 2022 marcelocripe <marcelocripe@gmail.com>
License: GPL-3+

License: GPL-2+
  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.
  .
  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.
  .
  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
  .
  On Debian systems, the complete text of the GNU General Public License,
  version 2, can be found in /usr/share/common-licenses/GPL-2.

License: GPL-3+
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  .
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  .
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
  .
  On Debian systems, the complete text of the GNU General Public License,
  version 3, can be found in /usr/share/common-licenses/GPL-3.

License: GFDL-1.3+
  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3 or
  any later version published by the Free Software Foundation;  with no
  Invariant Sections, no Front-Cover Texts, and no Back-Cover  Texts.
  A copy of the license should be found with this document. If you did
  not obtain the license, it can be found at
  https://www.gnu.org/licenses/licenses.html#FDL
  .
  On Debian systems, the complete text of the GNU Free Document License,
  version 1.3 can be found in /usr/share/common-licenses/GFDL-1.3.
