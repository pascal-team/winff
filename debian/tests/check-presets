#!/bin/sh

# Script to test the presets. It test if the presets can be run
# on the current version of ffmpeg. The functional presets are
# copied to a new file.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Copyright: Paul Gevers <paul@climbing.nl>, 2008-2013

# Stop if an error occurs
set -e

# Default variables/properties (will be overridden when parameters are used).
PRESET_FILE="debian/presets.xml"
TEST_FILE=`pwd`"/debian/tests/test.avi"
TEMP_DIR=`mktemp --directory --tmpdir winff.XXXXXX`
if [ -z "$FFMPEG" ] ; then
	FFMPEG=`which avconv` || echo "Unable to find avconv"
fi
if [ -z "$FFMPEG" ] ; then
	FFMPEG=`which ffmpeg` || echo "Unable to find ffmpeg"
fi

# Function for showing help text how to use this program
helpText()
{
    echo ""
    echo
    echo "    $0 "
    echo "        Test script to check if the specified executable can process the"
    echo "        the specified test file with all the presets available."
    echo
    echo "        -c"
    echo "        --command   : The location of executable to test."
    echo "        -d"
    echo "        --dir       : The location of the temporary work directory."
    echo "        -f"
    echo "        --file      : The location of the presets-file."
    echo "        -t"
    echo "        --test      : The location of the test movie."
    echo "        -h"
    echo "        --help      : This help text"
    echo
    echo "    Example: $0 --command=$FFMPEG --dir=$TEMP_DIR --file=$PRESET_FILE --test=$TEST_FILE"
    echo
}


# Check for parameters and convert them to the correct variables
# Parse arguments
TEMP=$(getopt -n check_presets \
--options c:,d:,f:,t:,s,h \
--longoptions command:,dir:,file:,test:,sort-only,help \
-- "$@")

eval set -- "$TEMP"
while true; do
    case $1 in 
        -c | --command)
            FFMPEG="$2"
	    shift
            ;;
	-d | --dir)
	    TEMP_DIR="$2"
	    shift
            ;;
	-f | --file )
	    PRESET_FILE="$2"
	    shift
            ;;
	-t | --test )
	    TEST_FILE="$2"
	    shift
            ;;
	-s | --sort-only )
	    SORT_ONLY="true"
            ;;
	-h | --help )
	    helpText
            exit 0
	    ;;
        --)                                                                 
            # no more arguments to parse                                
            break                                                       
            ;;    
	*   )
	    echo Unknown parameter or inproper use: "$PARAM"
            helpText
            exit 0
	    ;;
    esac
    shift
done

# Test if work directory exist; else make it
if [ ! -d $TEMP_DIR ] ; then
    echo -n "Working directory \"${TEMP_DIR}\" did not exist, creating it..."
    mkdir $TEMP_DIR
    echo "done."
fi
if [ ! -d ${TEMP_DIR}/logs ] ; then
    echo -n "Creating \"${TEMP_DIR}/logs\" ..."
    mkdir ${TEMP_DIR}/logs
    echo "done."
fi

# Copy the preset file to the working directory so that all the work can be done locally
cp ${PRESET_FILE} ${TEMP_DIR} 

# Some cleanup of the variables (and stripping off the path of the preset file).
TEMP_DIR=${TEMP_DIR%/}
PRESET_FILE=${PRESET_FILE##*/}

# Test if xmlstarlet exists
if [ ! -x $(which xmlstarlet) ] ; then
    echo "For this script to run succesfully you need to install xmlstarlet."
    exit 1
fi

# Move over to the working directory
cd $TEMP_DIR

# Create batch file to run ffmpeg/avconv on all presets
XMLLIST=`xmlstarlet el -d2 "$PRESET_FILE"`
for LINE in $XMLLIST ; do
	echo $LINE
    if [ "${#LINE}" -gt 8 ] ; then
		PRESET="${LINE#presets/}"
		LABEL=`xmlstarlet sel -T -t -m "$LINE" -v label "$PRESET_FILE"`
		PARAMS=`xmlstarlet sel -T -t -m "$LINE" -v params "$PRESET_FILE"`
		EXTENSION=`xmlstarlet sel -T -t -m "$LINE" -v extension -o "|" "$PRESET_FILE"`
		CATEGORY=`xmlstarlet sel -T -t -m "$LINE" -v category "$PRESET_FILE"`
		EXTENSION="${EXTENSION%|}"
		EXTENSION_TEST=`echo "$EXTENSION" | tr -s \| _`

		if [ "$EXTENSION_TEST" != "$EXTENSION" ]; then
			echo
			echo "###########################################"
			echo
			echo "   WARNING: Preset $PRESET is not unique."
			echo
			echo "###########################################"
			echo
			exit 1
		else
			echo Processing preset "$PRESET"
			$FFMPEG -i ${TEST_FILE} $PARAMS -y -t 1 test.$EXTENSION
			echo Processing preset "$PRESET" finished successfully
		fi
    fi
done
